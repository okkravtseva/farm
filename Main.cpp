#include <iostream>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <queue>
#include <chrono>


using namespace std;

int money_for_one_farm = 15;
int max_farms = 5;

int current_num_of_farms = 0;
int current_cash_account = 0;
int construction_company_account = 0;
bool first_time = true;

mutex mut, hlp_mut; // hlp_mut for stream output
condition_variable data_cond; // Condition variable
bool ready = false;

void making_money(int sec, int farm_cost)
{
	while (current_num_of_farms < max_farms)
	{
		lock_guard<mutex> lk(mut);
		std::this_thread::sleep_for(chrono::milliseconds(sec));
		++current_cash_account;
		{
			lock_guard<mutex> guard(hlp_mut);
			cout << "Thread 1, current_cash_account: " << current_cash_account << endl;
		}
		if (current_cash_account == money_for_one_farm)
		{
			construction_company_account += current_cash_account;
			current_cash_account = 0;
			if (first_time) // Ready to start construction
			{
				first_time = false;
				ready = true;
				data_cond.notify_one(); // ��������� ��������� �����
			}
		}
//		if (current_num_of_farms >= max_farms)
//			break;
	}
}

void farm_construction(int sec)
{
	unique_lock<std::mutex> lk(mut);
	data_cond.wait(lk, [] {return ready;}); // �������� ������-���������� � ������ �������, ���������� ������������ �������
		// ���� ������� true, �� wait �����������
		// ���� false, �� ������� ������������� � ����� �������
	lk.unlock();
	while (current_num_of_farms < max_farms)
	{
		this_thread::sleep_for(chrono::milliseconds(sec));
		++current_num_of_farms;
		{
			lock_guard<mutex> guard(hlp_mut);
			cout << "Thread 2, current_num_of_farms: " << current_num_of_farms << endl;
		}
	}
};

int main()
{
	thread t1(making_money, 3000, money_for_one_farm); // 30
	thread t2(farm_construction, 60000); // 600

	t1.join();
	t2.join();
}